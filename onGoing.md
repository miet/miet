# Bugs & Work in progress

Improvement:
* miet.expand needs to be respecified... and rename miet.gather ?

Bugs:

* mVol with special symbol in its name (-/+///...)
* mVol cannot be named label in miet.extract (collision with var names)

TODO:

* reorganise arguments for miet.extract and miet.apply 
* refactor miet.apply so that the function is passed as string and uses mVol and mKey as arguments => f=function(mtr){which('[[mtr]]'==2)}; mtr='BLABAL';stringr::str_glue(paste0(deparse(f),collapse=' '),.open="\"[[",.close="]]\"")
* function miet.filterFromData needs to be think again, the question is how....

todo:

* THINK ABOUT IT AGAIN ... The management of uniqueId has to be think again. fullId would be a better name. Make systematic the use of the mietCommon functions getUniqueId, getPath, ..
* xCord.. should be given in [[...]] and built only if needed
* reindent and revise code consitency
* miet.checkConsistency and miet.checkPopConsistency to implement using oro.nii
* check volume compatibility (geometry and frame) in miet.extract
* precondition for miet.expand
