% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/mietMPop.R
\name{new.mPop}
\alias{new.mPop}
\title{Create mPop object}
\usage{
new.mPop(t, root, cmd, path = "path")
}
\arguments{
\item{t}{Tibble data}

\item{root}{String representing the root folder(s) for which the mPop has been obtained}

\item{cmd}{String representing how the mPop has been obtained}

\item{path}{Vector of string representing the path variables}
}
\value{
a mPop object
}
\description{
Create mPop object
}
