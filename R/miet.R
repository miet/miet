#' miet: Medical Imaging Extraction Tools
#'
#'Miet is designed to specify and extract data frame ready for data analysis from a specified folder hierarchy containing MRI volumes. 
#'Miet abtractizes iterations over acquisition sets and MRI files reading, allowing to focus code writing and reading on definition of regions of interest and of statistics specification. 
#'The way miet proceed is: 
#'1, Populate: Given a description of how the folders are structured, build a data frame gathering information about data hierarchy and location.
#'2, Extract: Build a data frame containing, for each element of the population, values extracted from MRIs on a variety of user-defined ROIs. 
#'3, Expand: all these pieces are merged together to you end-up with a complete dataframe ready for explanatory analysis or hypothesis testing.
#'
#' Please see the joss paper for a more detailed introduction and additional resources (https://joss.theoj.org/papers/812bd13a18e338ef53c6760c32e6216c)
#'
#' @keywords internal
#' @docType package
#' @name miet
NULL

globalVariables(c("path", "metric","value","dummyVol","roiId","uniqueId","toKeep","pred","."))