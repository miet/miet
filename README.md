

# ![ ](rsc/miet.png "miet hex") Medical Imaging Extraction Tools

## Overview

Miet is an R package providing a set of functions to specify and extract data
frames or tibbles ready for data analysis from populations of MR images. 

## Purpose

In plenty of situations, an MRI study is stored as a hierarchy of folders
containing MRI data. Such study may contain a variety of
subject/subject-type/center/timeStep/sequences... that structures the
corresponding folders naming and hierarchy. Here is an example of such
structure (obvisouly, other ways of structuring the same dataset could be
considered):

[//]: # (Generated using: tree -L 3 --charset=ascii)
[//]: # (The first folder represents the center of acquisitions, the second represents the subject identifier and the last represents the time-step associated to these acquisitions.)
[//]: # (Then you end-up with a complete data-frame ready for explanatory analysis or hypothesis testing.)
[//]: # (ls dataTest/*/*/mtr.nii.gz | sed 's/.nii.gz//'| awk '{print "animaCropImage -i "$1".nii.gz --xindex 120 --xsize 80 --yindex 120 --ysize 80 --zindex 12 --zsize 40  -o "$1"Sub.nii.gz"}'|sh)

```
|-- center01
|   |-- control01
|   |   `-- M0
|   |-- control02
|   |   `-- M0
|   |-- patient01
|   |   |-- M0
|   |   `-- M12
|   `-- patient02
|       |-- M0
|       `-- M12
`-- center02
    |-- control01
    |   |-- M0
    |   `-- M12
    |-- control02
    |   |-- M0
    |   `-- M12
    |-- patient01
    |   |-- M0
    |   `-- M12
    `-- patient02
        |-- M0
        `-- M12
```


Each of the final folder contains a set of nifti files for the given subject
at the given time step, in the given center. These files may consist of a set
of raw data as well as post-processed data such as segmentation masks,
co-registered volumes, ... that are produced from a variety of image
processing tools.

Once all this data produced, the next step generally consists in analyzing
them. An ubiquitous analysis in medical imaging is the so-called
region-of-interest based analysis that consists in analyzing statistics of MR
signals over sets of predefined regions [(Poldrack07)][1].

Miet is designed to specify and extract data frames ready for data analysis
from a specified folder hierarchy and set of extraction formulas. 


## Installation & Dependences


Miet is fully written in R and is based on the following packages:

* stringr
* dplyr
* tidyr

Moreover, nii and nii.gz reading needs the following package:

* oro.nifti
 
To install miet, simply type in an R console (after having installed the `devtools` package, e.g. `install.packages('devtools')`):

```R
devtools::install_git("https://gitlab.inria.fr/miet/miet.git")
```


Once installed, you can check that miet is properly installed typing:
```R
miet::miet_simpleTest(T)
```

that may return
```
Joining, by = "uniqueId"
Joining, by = "subjectId"
# A tibble: 20 x 9
   path          uniqueId centerId subjectId mean_value sex     age  size weight
   <chr>         <chr>    <chr>    <chr>          <dbl> <chr> <dbl> <dbl>  <dbl>
 1 /home/… center0… center01 01              35.3 F        31  1.6      68
 2 /home/… center0… center01 02              35.2 M        34  1.7      80
 3 /home/… center0… center01 03              36.5 F        21  1.55     50
 4 /home/… center0… center01 04              34.7 M        27  1.73     62
 5 /home/… center0… center01 05              35.4 F        36  1.63     65
...
```


## A simple example


A detailed example is provided in the paper [Combès, (2020). miet: an R package for region of interest analysis from magnetic reasonance images. Journal of Open Source Software, 5(45), 1862](https://doi.org/10.21105/joss.01862).

Below is summarised a similar example with synthetic data provided with the
`miet` package. Let consider the following simple folder structure:

```
|-- center01
|   |-- 01
|   |-- 02
|   |-- 03
|   `--....
`-- center02
    |-- 01
    |-- 02
    |-- 03
    `--....
```

  Moreover, let consider that each of the final folder contains (among others)
the files `value.nii.gz` and `vLabel.nii.gz`. Our purpose, in this
example, consists in studying the difference of mean MTR values for voxels
corresponding to vertebral levels C1 to C3 (so that voxel of 
`vLabel.nii.gz`=1,2 or 3) between center 01 and 02.

The way miet proceed is:

1. Populate: Given a description of how the folders are structured, build a tibble gathering information about data hierarchy and location (if you do not know [tibbles][2], just consider them as improved data.frame structure ; if you don't know data.frame, just consider them as a spreadsheet-like data structure).
2. Extract/Apply: Build a tibble that contains, for each element from your population, values extracted from the associated MRI volumes.
3. Expand: Gather information from a set of generated tibbles into a single tibble.


## Populate


We gonna parse the folder structure using the function `miet_populate`:
```R
library(miet)

 ## this is the root folder location
ROOT=system.file("extdata", "simpleExample", package = "miet")

## this is how to data are structured
pattern='{{centerId}}/{{subjectId}}'

## This documents each element of the previous pattern
mEnv=new.env() 
mEnv$centerId='center\\d{2}' #centerId is center followed by 2 digits
mEnv$subjectId='\\d{2}' #2 digits

pop=miet_populate(ROOT,pattern,mEnv)
print(pop)

# A tibble: 20 x 2
#   centerId subjectId
#   <chr>    <chr>    
# 1 center01 01       
# 2 center01 02       
# 3 center01 03       
# 4 center01 04       
# 5 center01 05       
```

## Extracting

 We gonna extract the mean of volume `value.nii.gz` over a the set of voxels of
`vLabel.nii.gz` which equals 1,2 or 3 using the function `miet_extract`:

```


MTRC1C3=miet_extract(pop,formulaLabel='which([[vLabel]] %in% seq(1,3))',
                    '[[value]]',statFunction=mean)
print(MTRC1C3)

# A tibble: 20 x 3
#   metric      uniqueId    value
#   <chr>       <chr>       <dbl>
# 1 mean_mtr center01_01  36.6
# 2 mean_mtr center01_02  34.8
# 3 mean_mtr center01_03  35.7
# 4 mean_mtr center01_04  37.0
# 5 mean_mtr center01_05  35.7
# ...

```
## Extracting

Then consider that we want to include in our data subject demographic characteritics. Such data may for example be stored in a csv file:

```
characteristics=read.csv(paste0(ROOT,'/characteristic.csv'),
                         colClasses=c('character','character','numeric','numeric','numeric'))

print(characteristics)

#   subjectId sex age size weight
#1         01   F  31 1.60     68
#2         02   M  34 1.70     80
#3         03   F  21 1.55     50
#4         04   M  27 1.73     62
#5         05   F  36 1.63     65
#...
```

The next step is to gather all the computed elements in a single tibble. 
`miet_expand` is designed for such a purpose.

```
expandedDf=miet_expand(pop,MTRC1C3,
                       characteristics)
head(expandedDf)

# A tibble: 6 x 9
#  uniqueId centerId subjectId mean_mtr    sex     age  size weight
#  <chr>    <chr>    <chr>           <dbl> <chr> <dbl> <dbl>  <dbl>
#1 center0… center01 01               36.6 F        31  1.6      68
#2 center0… center01 02               34.8 M        34  1.7      80
#3 center0… center01 03               35.7 F        21  1.55     50
#4 center0… center01 04               37.0 M        27  1.73     62
#5 center0… center01 05               35.7 F        36  1.63     65
#...

```

This new tibble is now waiting for you to be visualised and analysed with standard R tools e.g.
```R
library(ggplot2)
library(dplyr)


expandedDf %>% group_by(centerId) %>% 
           summarise(mu=mean(mean_value),sigma=sd(mean_value))

# A tibble: 2 x 3
#  centerId    mu sigma
#  <chr>    <dbl> <dbl>
#1 center01  38.0  1.20
#2 center02  39.3  2.18


summary(lm(data=expandedDf,mean_value~centerId+subjectId))

#...
#Coefficients:
#                 Estimate Std. Error t value Pr(>|t|)    
#(Intercept)       38.5388     0.7427  51.889 2.11e-11 ***
#centerIdcenter02   1.5852     0.4697   3.375  0.00972 ** 

ggplot(expandedDf,aes(x=subjectId,y=mean_value))+geom_point(aes(color=centerId,size=weight/size^2))
```

![ ](rsc/ggp2.png "plot output")

**To conclude this example,** I would like to underline that miet avoided us to explicity:

* write loops over patient/acquisition/...
* deal with MRI files

and thus allows code writing and reviewing to focus on the statistics of interest :)

This example is intentionnally designed to be mininal: all default arguments
matchs well with the situation. Obvisouly, miet provides ways to deal with a
variety of other situations. See the [miet vignette][4] for
more.



# Contributing

There is definitely room for improvement and any enthousiastic person is welcomed to contribute to miet.

[//]: # (Below are a few feature that are planned in near future.## Performance ## User interaction)

Below are a few feature that are planned in near future:

* helper functions for `miet_populate` to deal with the [BIDS format][5].
* function to identify and summarize missing/corrupted/size-uncompatible MR data.
* function to generate "blank" csv file, so that the user can manually specify data that must be removed from the analysis. 
* progress bar for large data sets processing (that can be time consuming for complex ROI rules and/or large data sets).

# Participation guidelines

Please adhere to [Contributor Covenant](http://contributor-covenant.org) code of conduct for in any interactions you have within this project.


# License

Miet is open source software made available under the MIT license.

# Credits

If miet helps you, feel free to cite the compagnon joss paper:

```
Combès, (2020). miet: an R package for region of interest analysis from magnetic reasonance images. Journal of Open Source Software, 5(45), 1862
```
[![DOI](https://joss.theoj.org/papers/10.21105/joss.01862/status.svg)](https://doi.org/10.21105/joss.01862)




[1]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2555436/pdf/nsm006.pdf
[2]: https://cran.r-project.org/web/packages/tibble/vignettes/tibble.html
[3]: https://stringr.tidyverse.org/articles/regular-expressions.html
[4]: https://gitlab.inria.fr/miet/miet/blob/master/doc/vignette.pdf
[5]: https://bids.neuroimaging.io
